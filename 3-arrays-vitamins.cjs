/*
const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];
*/

//1. Get all items that are available 
function availableItems(data)
{
    return data.filter((item)=>item.available);
}

//console.log(availableItems(items));

//2. Get all items containing only Vitamin C
function onlyVitaminCItems(data)
{
    return data.filter((item)=>item.contains=="Vitamin C");
}

//console.log(onlyVitaminCItems(items));

//3.Get all items containing Vitamin A.
function vitaminAitems(data)
{
    return data.filter((item)=>{
        if(item.contains.includes("Vitamin A"))
            return item;
    })
}
//console.log(vitaminAitems(items));

//4. Group items based on the Vitamins
function groupByVitamins(data)
{
    let res = data.reduce((acc,item)=>{
        let arr = item.contains.split(', ');
        arr.map((ele)=>{
            if(acc.hasOwnProperty(ele))
            {
                acc[ele].push(item.name);
            }
            else
            {
                acc[ele] = [];
                acc[ele].push(item.name);
            }
        })
        return acc;
    },{});
    return res;
}

//console.log(groupByVitamins(items));

//5. Sort items based on number of Vitamins they contain.
function sortItemsByVitaminsCount(data)
{
    return data.sort(function(a,b){
        let aVitamins = a.contains.split(', ');
        let bVitamins = b.contains.split(', ');
        if(aVitamins.length>bVitamins.length)
            return 1;
        else if(aVitamins.length<bVitamins.length)
            return -1;
        else
            return 0;
    });
}

//console.log(sortItemsByVitaminsCount(items));